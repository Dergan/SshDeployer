﻿using SshDeployerLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Configuration;

namespace SshDeployerWebAPI.Controllers
{
    public class DeployController: ApiController
    {
        [HttpGet]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage GetConnectConfig(string Name, string RequestKey)
        {
            using (DataClassesDataContext context = new DataClassesDataContext(ConfigurationManager.ConnectionStrings["SshDeployerConnectionString"].ConnectionString))
            {
                User user = context.Users.FirstOrDefault(o => o.Name == Name && o.RequestKey == RequestKey);

                if(user == null)
                {
                    return base.Request.CreateResponse(System.Net.HttpStatusCode.NotAcceptable);
                }

                ConnectConfigModel configModel = new ConnectConfigModel()
                {
                    SshHost = user?.ServerConfig.SshHost,
                    SshHostPort = user?.ServerConfig.SshHostPort ?? 0,
                    SshServerAuthFingerPrint = user?.ServerConfig.SshAuthFingerPrint,

                    SshAuthUsername = user.SshAuthUsername,
                    SshAuthPassword = user.SshAuthPassword,
                    SshAuthPublicKey = user.SshAuthPublicKey,
                    SshUserAuthFingerPrint = user.SshAuthFingerPrint,

                    PortForwards = user.PortForwards.Select(pf => new PortForwardModel
                    {
                        LocalPort = pf.LocalPort,
                        RemoteHost = pf.RemoteHost,
                        RemotePort = pf.RemotePort,
                    }).ToList()
                };
                return base.Request.CreateResponse(System.Net.HttpStatusCode.OK, configModel);
            }
        }
    }
}