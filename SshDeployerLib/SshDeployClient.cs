﻿using Renci.SshNet;
using Renci.SshNet.Common;
using SshDeployerLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SshDeployerLib
{
    public class SshDeployClient
    {
        private SshClient Client;
        private ConnectConfigModel config;

        private string RequestUrl { get; set; }
        private string Name { get; set; }
        private string RequestKey { get; set; }


        public bool IsConnected
        {
            get
            {
                return Client?.IsConnected ?? false;
            }
        }

        internal SshDeployClient(ConnectConfigModel config, string RequestUrl, string Name, string RequestKey)
        {
            this.config = config;
            this.RequestUrl = RequestUrl;
            this.Name = Name;
            this.RequestKey = RequestKey;

            Client = new SshClient(config.SshHost, config.SshHostPort, config.SshAuthUsername, config.SshAuthPassword);
            Client.HostKeyReceived += Client_HostKeyReceived;

            Client.Connect();

            ThreadPool.QueueUserWorkItem(PortforwardingThread);
        }

        private void Client_HostKeyReceived(object sender, Renci.SshNet.Common.HostKeyEventArgs e)
        {
            string FingerPrint = BitConverter.ToString(e.FingerPrint).Replace('-', ':');

            e.CanTrust = FingerPrint == config.SshServerAuthFingerPrint || FingerPrint == config.SshUserAuthFingerPrint;
        }

        public void Disconnect()
        {
            Client?.Disconnect();
        }

        private void PortforwardingThread(object o)
        {
            while(IsConnected)
            {
                config = SshDeployer.GetConfig(RequestUrl, Name, RequestKey);

                List<PortForwardModel> NeedToForward = config.PortForwards.Where(pf =>
                {
                    return !Client.ForwardedPorts.Any(forward =>
                    {
                        ForwardedPortRemote remote = forward as ForwardedPortRemote;
                        return (uint)remote.BoundPort == pf.LocalPort && remote.Host == pf.RemoteHost && remote.Port == pf.RemotePort;
                    });
                }).ToList();

                Client.ForwardedPorts.Where(forward =>
                {
                    ForwardedPortRemote remote = forward as ForwardedPortRemote;

                    return !config.PortForwards.Any(pf =>
                    {
                        return (uint)remote.BoundPort == pf.LocalPort && remote.Host == pf.RemoteHost && remote.Port == pf.RemotePort;
                    });
                }).ToList().ForEach(forward =>
                {
                    Client.RemoveForwardedPort(forward);

                    //ForwardedPortRemote remote = forward as ForwardedPortRemote;
                    //Console.WriteLine($"Removed Forwarding {remote.BoundPort}:{remote.Host}:{remote.Port}");
                });


                NeedToForward.ForEach(pf =>
                {
                    try
                    {
                        Client.AddForwardedPort(new ForwardedPortRemote("127.0.0.1", (uint)pf.LocalPort, pf.RemoteHost, (uint)pf.RemotePort));
                        Console.WriteLine($"Forwarding {pf.LocalPort}:{pf.RemoteHost}:{pf.RemotePort}");
                    }
                    catch(SshConnectionException)
                    {
                        //can occur when SSH connection is lost
                    }
                });
                Client.ForwardedPorts.Where(pf => !pf.IsStarted).ToList().ForEach(pf => pf.Start());

                Thread.Sleep(TimeSpan.FromMinutes(5));
            }
        }
    }
}
