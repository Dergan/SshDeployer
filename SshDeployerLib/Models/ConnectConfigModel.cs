﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SshDeployerLib.Models
{
    public class ConnectConfigModel
    {
        public string SshHost { get; set; }
        public int SshHostPort { get; set; }
        public string SshAuthUsername { get; set; }
        public string SshAuthPassword { get; set; }
        public string SshAuthPublicKey { get; set; }
        public string SshServerAuthFingerPrint { get; set; }
        public string SshUserAuthFingerPrint { get; set; }

        public List<PortForwardModel> PortForwards { get; set; }
    }
}
