﻿using RestSharp;
using SshDeployerLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SshDeployerLib
{
    public class SshDeployer
    {

        public static ConnectConfigModel GetConfig(string RequestUrl, string Name, string RequestKey)
        {
            RestClient client = new RestClient(RequestUrl);

            var request = new RestRequest("api/Deploy/GetConnectConfig", Method.GET);
            request.AddQueryParameter("Name", Name);
            request.AddQueryParameter("RequestKey", RequestKey);
            IRestResponse<ConnectConfigModel> response = client.Execute<ConnectConfigModel>(request);
            return response.Data;
        }

        public static SshDeployClient Connect(string RequestUrl, string Name, string RequestKey)
        {
            return new SshDeployClient(GetConfig(RequestUrl, Name, RequestKey), RequestUrl, Name, RequestKey);
        }
    }
}
