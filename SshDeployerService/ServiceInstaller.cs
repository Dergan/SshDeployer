﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SshDeployerService
{
    [RunInstaller(true)]
    public class MyWindowsServiceInstaller : Installer
    {
        string strServiceName = "SshDeployerService";

        public MyWindowsServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = strServiceName;
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = strServiceName;
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }

        void ServiceInstaller_Committed(object sender, InstallEventArgs e)
        {
            // Auto Start the Service Once Installation is Finished.
            var controller = new ServiceController(strServiceName);
            controller.Start();
        }
    }

    //[RunInstaller(true)]
    //public class ServiceInstaller : Installer
    //{
    //    string strServiceName = "MyServiceName";

    //    public ServiceInstaller()
    //    {
    //        var processInstaller = new ServiceProcessInstaller();
    //        var serviceInstaller = new ServiceInstaller();

    //        processInstaller.Account = ServiceAccount.LocalSystem;
    //        processInstaller.Username = null;
    //        processInstaller.Password = null;

    //        serviceInstaller.DisplayName = strServiceName;
    //        serviceInstaller.StartType = ServiceStartMode.Automatic;

    //        serviceInstaller.ServiceName = strServiceName;

    //        this.Installers.Add(processInstaller);
    //        this.Installers.Add(serviceInstaller);

    //        this.Committed += new InstallEventHandler(ServiceInstaller_Committed);
    //    }

    //    void ServiceInstaller_Committed(object sender, InstallEventArgs e)
    //    {
    //        // Auto Start the Service Once Installation is Finished.
    //        var controller = new ServiceController(strServiceName);
    //        controller.Start();
    //    }
    //}
}
