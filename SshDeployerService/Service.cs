﻿using SshDeployerLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SshDeployerService
{
    public partial class Service : ServiceBase
    {
        private SshDeployClient Client;

        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ThreadPool.QueueUserWorkItem(onStartThread);
        }

        private void onStartThread(object o)
        {
            while (true)
            {
                try
                {
                    Client = SshDeployer.Connect(ConfigurationManager.AppSettings["RequestURL"],
                                                 ConfigurationManager.AppSettings["Name"],
                                                 ConfigurationManager.AppSettings["RequestKey"]);

                    //Wait till client is disconnected
                    while (Client?.IsConnected ?? false)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(5));
                    }
                }
                catch (Exception ex)
                {

                }

                Thread.Sleep(TimeSpan.FromMinutes(1));
            }
        }

        protected override void OnStop()
        {
            Client?.Disconnect();
        }
    }
}
