# SshDeployer

Deploy SSH PortForwarding like a Pro

# Features
* Multi-User support
* SSH Fingerprint checks
* Easy to setup with 3 MsSQL Tables
* SSH Local PortForwarding
* WebAPI as frontend for getting configuration
* Persistant SSH Portforwarding with Windows Services
* Can use already existing SSH Servers

# Libs used
* SSH.Net (https://github.com/sshnet/SSH.NET)
* Newtonsoft JSON
* RestSharp

![](SshDiagram.png)