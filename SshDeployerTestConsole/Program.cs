﻿using Newtonsoft.Json;
using SshDeployerLib;
using SshDeployerLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SshDeployerTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string RequestUrl = "http://localhost:7128/";
            string Name = "Test";
            string RequestKey = "69234f5jy789f23459678j2345f678j90345";

            ConnectConfigModel config = SshDeployer.GetConfig(RequestUrl, Name, RequestKey);

            if (config != null && !String.IsNullOrWhiteSpace(config.SshHost))
            {
                Console.WriteLine("Pulled config");
                Console.WriteLine(JsonConvert.SerializeObject(config, Formatting.Indented));

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Connected");

                SshDeployer.Connect(RequestUrl, Name, RequestKey);
            }
            else
            {
                Console.WriteLine("Failed to get config");
            }

            Console.ReadLine();
        }
    }
}
